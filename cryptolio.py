import requests
import lxml.html
from urllib.parse import urljoin
from flask import Flask, jsonify
from flask_restful import Resource, Api
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

app = Flask(__name__)
api = Api(app)
limiter = Limiter(app, key_func=get_remote_address)

SOURCE = 'https://coinmarketcap.com/'


class Index(Resource):
    def get(self):
        return {'Cryptolio API': {
                'version': 1.0,
                'endpoints': {
                    'coins/': 'List of top 100 crypto coins',
                    'coins/<string:coin>': 'Statistics of a coin. Usage: e.g. coins/Bitcoin'
                }
            }
        }


class CoinsAvailable(Resource):
    decorators = [
        limiter.limit("20/minute")
    ]

    def get(self):
        r = requests.get(SOURCE)
        if not r.status_code == 200:
            return {'Status Code:': f'{r.status_code}'}
        
        html = lxml.html.fromstring(r.content)
        coins = html.xpath('//table//tr//td//@title')

        return jsonify(coins)


class CoinDetails(Resource):
    decorators = [
        limiter.limit("10/minute")
    ]

    def get(self, coin):
        r = requests.get(SOURCE)
        if not r.status_code == 200:
            return {'Status Code:': f'{r.status_code}'}
        
        html = lxml.html.fromstring(r.content)
        coin_source = html.xpath(f"//table//a[@title='{coin}']/@href")[0]

        r = requests.get(urljoin(SOURCE, coin_source))
        if not r.status_code == 200:
            return {'Status Code:': f'{r.status_code}'}
        
        html = lxml.html.fromstring(r.content)
        coin_stats = html.xpath("//div[@class='cmc-details-panel-about__table']/div")

        stats = {}
        for stat in coin_stats:
            stats[stat[0].text_content()] = stat[1].text_content()

        return jsonify(stats)


api.add_resource(Index, '/')
api.add_resource(CoinsAvailable, '/coins/')
api.add_resource(CoinDetails, '/coins/<string:coin>/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
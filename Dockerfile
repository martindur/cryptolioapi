FROM python:3.8.0-alpine

ENV PYTHONUNBUFFERED 1

COPY . /usr/src/app/cryptolioAPI/

WORKDIR /usr/src/app/cryptolioAPI/

#Dependencies for lxml
RUN apk add --no-cache --virtual .build-deps gcc libc-dev libxslt-dev && \
    apk add --no-cache libxslt && \
    pip install --no-cache-dir lxml==4.5.0 && \
    apk del .build-deps

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 5005